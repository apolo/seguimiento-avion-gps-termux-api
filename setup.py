from distutils.core import setup
setup(
  name = 'gps-tools',
  packages = ['gps-tools'], # this must be the same as the name above
  version = '0.1',
  description = 'paquete para creacion de mapas usando coordenadas gps',
  author = 'Julian Guillermo Zapata Rugeles',
  author_email = 'julianruggeles@gmail.com',
  url = 'https://salsa.debian.org/apolo/seguimiento-avion-gps-termux-api', # use the URL to the github repo
  download_url = 'https://salsa.debian.org/apolo/seguimiento-avion-gps-termux-api/tarball/0.1',
  keywords = ['gps', 'graphics', 'coordenadas'],
  install_requires=[
        'folium'
    ]
)